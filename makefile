CC=gcc
CFLAGS=-lsodium

.PHONY: default
default: class-randomizer-3

%: %.c
	$(CC) -o $@ $< $(CFLAGS)
